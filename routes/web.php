<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register', function () {
    return view('register');
});

Route::get('/home', function () {
    return view('home');
});



//Route::get('/dashboard', 'Admin\Dashboard_cont@index')->middleware('auth');


Auth::routes();

Route::post('/logout', 'web\Dashboard_cont@index');
Route::get('/myorders', 'web\Device_cont@index');
Route::get('/mycart', 'web\Device_cont@index');
Route::get('/checkout', 'web\Device_cont@index');
Route::get('/details/{id}', 'web\productweb_cont@index');
Route::get('/', 'web\homepage@index');




Route::group(['middleware' => 'role:admin'], function() {
    /*Route::get('/dashboard', function() {
        return 'Welcome Admin';
    });*/

    //Route::get('/', 'Admin\Dashboard_cont@index');
    Route::get('/admin', 'Admin\Dashboard_cont@index');

    Route::get('/admin/product', 'Admin\product_cont@index');
    Route::get('admin/product/create', 'Admin\product_cont@create')->name('product.create');
    Route::post('admin/product/create', 'Admin\product_cont@store')->name('product.store');
    Route::get('admin/product/edit/{id}', 'Admin\product_cont@edit')->name('product.edit');
    Route::post('admin/product/update', 'Admin\product_cont@update')->name('product.update');
    Route::get('admin/product/update', 'Admin\product_cont@update')->name('product.update2');
    Route::get('admin/product/delete/{id}', 'Admin\product_cont@delete')->name('product.delete');


    Route::get('/admin/users', 'Admin\product_cont@index');
    Route::get('admin/users/create', 'Admin\product_cont@create')->name('users.create');
    Route::post('admin/users/create', 'Admin\product_cont@store')->name('users.store');
    Route::get('admin/users/edit/{id}', 'Admin\product_cont@edit')->name('users.edit');
    Route::post('admin/users/update', 'Admin\product_cont@update')->name('users.update');
    Route::get('admin/users/update', 'Admin\product_cont@update')->name('users.update2');
    Route::get('admin/users/delete/{id}', 'Admin\product_cont@delete')->name('users.delete');

    Route::get('/admin/order', 'Admin\order_cont@index');
    Route::get('admin/order/edit/{id}', 'Admin\order_cont@edit')->name('order.edit');
    Route::post('admin/order/update', 'Admin\order_cont@update')->name('order.update');
    Route::get('admin/order/update', 'Admin\order_cont@update')->name('order.update2');



});
