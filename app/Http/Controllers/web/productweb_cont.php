<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;

class productweb_cont extends Controller
{
    public function index($id , Request $request){
        $entity = Product::find($id);
        $additionaimages = product::find($id)->Productadditionalimages;

        return view('web.productdetails', compact( 'entity' , 'additionaimages' ));
    }
}
