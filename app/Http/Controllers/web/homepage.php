<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Product;

class homepage extends Controller
{
    public function index(Request $request){
        $list = DB::table('products')->orderBy('created_at', 'desc')->paginate(15);
        return view('web.home', compact('list'));
    }
}
