<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\device;

class Device_cont extends Controller
{
    public function index(Request $request){
        $list = Device::get();
        $name = $request->user()->name;
        $heading_title = "Projects";
        $subheading_title = "List";
        return view('admin.device_list', compact('name','list' , 'heading_title' , 'subheading_title'));
    }


    public function create(Request $request){
        $name = $request->user()->name;
        $mthoad = "1";
        $heading_title = "Project";
        $subheading_title = "Add";
        return view('admin.device_form', compact('name'  , 'mthoad' , 'heading_title' , 'subheading_title'));
    }

    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'projectname' => 'required',
            'projectno' => 'required',
            'missingparts' => 'required',
            'missingpartsno' => 'required',
            'notes' => 'required',
            'replacedparts' => 'required'
        ]);


        $missingparts = $request->input('missingparts');
        $dom = new \DomDocument();
        $dom->loadHtml($missingparts, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $missingparts = $dom->saveHTML();


        $notes = $request->input('notes');
        $dom = new \DomDocument();
        $dom->loadHtml($notes, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $notes = $dom->saveHTML();



        $replacedparts = $request->input('replacedparts');
        $dom = new \DomDocument();
        $dom->loadHtml($replacedparts, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }
        $replacedparts = $dom->saveHTML();

        $entity = new Device();
        $entity->projectname = $request->input('projectname');
        $entity->projectno = $request->input('projectno');
        $entity->missingparts = $missingparts;
        $entity->missingpartsno = $request->input('missingpartsno');
        $entity->notes = $notes;
        $entity->replacedparts = $replacedparts;

        $entity->save();
        return redirect("/admin/device");
    }


    public function update(Request $request)
    {
        $validate = $this->validate($request,[
            'projectname' => 'required',
            'projectno' => 'required',
            'missingparts' => 'required',
            'missingpartsno' => 'required',
            'notes' => 'required',
            'replacedparts' => 'required'
        ]);


        $missingparts = $request->input('missingparts');
        $dom = new \DomDocument();
        $dom->loadHtml($missingparts, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $missingparts = $dom->saveHTML();


        $notes = $request->input('notes');
        $dom = new \DomDocument();
        $dom->loadHtml($notes, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $notes = $dom->saveHTML();



        $replacedparts = $request->input('replacedparts');
        $dom = new \DomDocument();
        $dom->loadHtml($replacedparts, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }
        $replacedparts = $dom->saveHTML();

        $id = $request->input('id');
        $entity =  Device::find($id);
        $entity->projectname = $request->input('projectname');
        $entity->projectno = $request->input('projectno');
        $entity->missingparts = $missingparts;
        $entity->missingpartsno = $request->input('missingpartsno');
        $entity->notes = $notes;
        $entity->replacedparts = $replacedparts;

        $entity->save();
        return redirect()->back()->with('message', 'Success Updated!!');
    }


    public function edit($id , Request $request){
        $name = $request->user()->name;
        $entity = Device::find($id);
        $mthoad = "2";
        $heading_title = "Project  Manage";
        $subheading_title = "Edit";
        return view('admin.device_form', compact('name'  , 'mthoad' , 'entity'  , 'heading_title' , 'subheading_title'));
    }

    public function delete($id){
        $entity = Device::where('id',$id)->delete();
        return redirect()->back()->with('message', 'Success Deleted!!');
    }

}
