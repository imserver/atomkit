<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Order;

class order_cont extends Controller
{

    public function index(Request $request){
        $list = DB::table('orders')->paginate(5);
        $name = $request->user()->name;
        $heading_title = "Orders";
        $subheading_title = "List";
        return view('admin.orders_list', compact('name','list' , 'heading_title' , 'subheading_title'));
    }

    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required',
            'notes' => 'required',
            'qty' => 'required'
        ]);


        $description = $request->input('description');
        $dom = new \DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/photos/" . time().$k.'.png';
            $path = public_path() . $image_name;
            file_put_contents($path, $data);
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $description = $dom->saveHTML();



        $entity = new Product();
        $entity->name = $request->input('name');
        $entity->alias =str_slug($request->input('alias'), '-');
        $entity->description = $description;
        $entity->image = $request->input('image');
        $entity->price = $request->input('price');
        $entity->qty = $request->input('qty');

        $entity->save();
        return redirect("/admin/product");
    }

    public function details($id , Request $request){
        $name = $request->user()->name;
        $entity = Order::find($id);
        $mthoad = "2";
        $heading_title = "Order  Details";
        $subheading_title = "Edit";
        return view('admin.orders_form', compact('name'  , 'mthoad' , 'entity'  , 'heading_title' , 'subheading_title'));
    }

    public function delete($id){
        $entity = Order::where('id',$id)->delete();
        return redirect()->back()->with('message', 'Success Deleted!!');
    }
}
