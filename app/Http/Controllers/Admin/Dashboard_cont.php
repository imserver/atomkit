<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Product;
class Dashboard_cont extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){

        //dd($user->hasRole('admin')); //will return true, if user has role
      //  dd($user->givePermissionsTo('edit-users','create-tasks')); // will return permission, if not null
     //--   dd($user->can('create-tasks')); // will return true, if user has permissio
        if (Auth::check()) {
            $user = $request->user();
            if($user->hasRole('admin')) {
                $name = $user->name;
              //  $list = Product::get()->paginate(5);
                $list = DB::table('products')->orderBy('created_at', 'desc')->paginate(5);

                $heading_title = "Products";
                $subheading_title = "List";

                return view('admin.index', compact('name','list' , 'heading_title' , 'subheading_title'));
            }else{
                redirect('/login');
            }
        }else{
            redirect('/login');
        }
    }
}
