<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "carts";
    protected $fillable = [
        'user_id'
    ];




    public function products()
    {
        return $this->belongsToMany(Product::class);
    }


    public function User(){
        return $this->belongsTo('App\User','user_id');
    }

    public function order(){
        return $this->hasOne('App\Model\Order');
    }
}
