<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = [
        'name',
        'alias',
        'description',
        'image',
        'price',
        'qty',
        'category_id'
    ];


    public function Productadditionalimages(){
        return $this->hasMany('App\Model\Productadditionalimages');
    }


    public function carts()
    {
        return $this->belongsToMany(Cart::class);
    }

    public function Category()
    {
        return $this->belongsTo('App\Model\Category' ,'category_id');
    }
}
