<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        'cart_id',
        'total'
    ];


    public function cart(){
        return $this->belongsTo('App\Model\Cart' , 'cart_id');
    }
}
