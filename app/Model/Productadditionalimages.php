<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Productadditionalimages extends Model
{
    protected $table = "productadditionalimages";
    protected $fillable = [
        'image_url',
        'product_id'
    ];


    public function product(){
        return $this->belongsToMany('App\Model\Product' , 'product_id');
    }
}
