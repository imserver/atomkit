<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device extends Model
{
    protected $table = "devices";
    protected $fillable = [
        'projectname',
        'projectno',
        'missingparts',
        'missingpartsno',
        'notes',
        'replacedparts'
    ];
}
