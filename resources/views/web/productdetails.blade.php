@extends('web.layout.main')



@section('content')
      <div class="row">
            <div class="col-md-12">


                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif


            </div>

          <div class="detailsproduct">

              <div class="col-md-5">
                  @if(isset($entity))
                      <img src="{{$entity->image}}" style="max-width: 100%; " class="thumb" />
                  @endif
              </div>

              <div class="col-md-7" style="    min-height: 560px;">
                <p class="protitle">{{$entity->name}}</p>
                <p class="proprice">{{ number_format($entity->price, 2) }}JD</p>
                  <div class="proddetails">{!! $entity->description !!} </div>

                  <div class="prodadditionalimages">
                      @foreach($additionaimages as $image)
                          <img src="{{$image->image_url}}" width="80" style="border-radius: 12px; margin-bottom: 20px; margin-right: 10px"/>
                          @endforeach
                  </div>
                  <a type="button"  href="{{ URL('/details/'.$entity->id )}}" class="button"> Add to Cart</a>
              </div>
          </div>
        </div>

@endsection