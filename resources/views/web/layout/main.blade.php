<!DOCTYPE html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    {!! Html::style('adminstyle2/css/bootstrap.css') !!}
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    {!! Html::style('adminstyle2/css/style.css') !!}
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    {!! Html::style('adminstyle2/css/font.css') !!}
    {!! Html::style('adminstyle2/css/font-awesome.css') !!}

    <!-- //font-awesome icons -->
    {!! Html::script('adminstyle2/js/jquery2.0.3.min.js') !!}
    {!! Html::script('adminstyle2/js/modernizr.js') !!}
    {!! Html::script('adminstyle2/js/jquery.cookie.js') !!}
    {!! Html::script('adminstyle2/js/screenfull.js') !!}



    <script src="https://kit.fontawesome.com/4a6918c25b.js"></script>

</head>
<body class="dashboard-page web">


<section class="title-bar">
    <div class="container">
    <div class="logo">
        <h1><a href="/"><img src="/adminstyle2/images/opencart-logo-white.png" alt="" /></a></h1>
    </div>

        <aiv class="cart"><a href=""><i class="fas fa-shopping-cart"></i></a></aiv>
    </div>

    <div class="clearfix"> </div>
</section>





    <div class="container" style="    padding-bottom: 50px;     padding-top: 50px;">

        @yield('content')

    </div>
    <!-- footer -->

    <!-- //footer -->

<div class="footer">
    <p>
        <a href="#">Contact Us</a>
        <a href="#">Terms of services</a>
        <a href="#">Privacy Policy</a>
        <a href="#">Help/FAQ's</a>
        <a href="#">Customer Support</a>
    </p>
</div>
{!! Html::script('adminstyle2/js/bootstrap.js') !!}
{!! Html::script('adminstyle2/js/proton.js') !!}


<script>
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>

<!-- CKEditor init -->
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>
<script>
    $('textarea[name=ce]').ckeditor({
        height: 100,
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
</script>

<!-- TinyMCE init -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    var editor_config = {
        path_absolute : "",
        selector: "textarea[name=tm]",
        plugins: [
            "link image"
        ],
        relative_urls: false,
        height: 129,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>

<script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
</script>
<script>
    $('#lfm').filemanager('image', {prefix: route_prefix});
    $('#lfm2').filemanager('file', {prefix: route_prefix});
</script>

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 100,  //set editable area's height

            focus: true,
            disableResizeEditor: true

        });

        $('.note-statusbar').hide();
    });
</script>
<script>
    $(document).ready(function(){

        // Define function to open filemanager window
        var lfm = function(options, cb) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = cb;
        };

        // Define LFM summernote button
        var LFMButton = function(context) {
            var ui = $.summernote.ui;
            var button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'Insert image with filemanager',
                click: function() {

                    lfm({type: 'image', prefix: '/laravel-filemanager'}, function(url, path) {
                        context.invoke('insertImage', url);
                    });

                }
            });
            return button.render();
        };

        // Initialize summernote with LFM button in the popover button group
        // Please note that you can add this button to any other button group you'd like
        $('#summernote-editor').summernote({
            toolbar: [
                ['popovers', ['lfm']],
            ],

            codemirror: { // codemirror options
                theme: 'monokai'
            },
            buttons: {
                lfm: LFMButton
            }
        })
    });
</script>




</body>
</html>
