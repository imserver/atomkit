@extends('admin.layout.admin')

@section('content')
    <div class="white-box">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Product</li>
        </ol>

        <h3 class="box-title">{{$heading_title}}
            <a href="/admin/product/create" style="color: gray;border: 1px solid gray;" class="btn btn-default btn-sm pull-right">+Add Product</a></h3>

        <br/>

        <div class="row">
            <div class="col-md-12">
                <div class="panel">

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif


                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table">
                            <thead>
                            <tr>
                                <!--
                                <th width="70" class="text-center">#</th> -->
                                <th>Thumb</th>
                                <th>Product</th>
                                <th>Price</th>


                                <th width="300">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $list_item)
                                <tr>
                                    <!--
                            <td class="text-center">{{$list_item->id}}</td> -->
                                    <td>
                                        <a type="button"  href="{{ URL('admin/product/edit/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5">
                                            <img src="{{$list_item->image}}" width="200"/> </a></td>
                                    <td class="text-left">
                                        <a type="button"  href="{{ URL('admin/product/edit/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5">{{$list_item->name}}</a>
                                    </td>

                                    </td>

                                    <td>{{$list_item->price}}


                                    <td>
                                        <!--
                                        <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-key"></i></button> -->
                                        <a type="button" href="{{ URL('admin/product/delete/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="far fa-trash-alt"></i></a>
                                        <a type="button"  href="{{ URL('admin/product/edit/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="far fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        {{ $list->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection