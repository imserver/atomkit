@extends('admin.layout.admin')



@section('content')
    <div class="row">
        <div class="col-md-12">

            @if ($errors->any())
                <div class="panel">

                    <div class="panel-heading">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

        </div>
    </div>

    <div class="row">
        @if(isset($mthoad))
            @if($mthoad==1)
                <form method="post" action="{{ route('product.store') }}" class="form-horizontal" >
                    @elseif($mthoad==2)
                        <form method="post" action="{{ route('product.update') }}" class="form-horizontal" >
                            @endif
                            @endif
                            <div class="col-md-8">

                                    <!-- <form class="form-horizontal"> -->
                                    {{ csrf_field() }}
                                    @if(isset($entity))
                                        <input autocomplete="off" type="hidden" name="id" value="{{$entity->id}}" />
                                    @endif

                                    <div class="form-group col-md-6" >

                                        <div class="col-md-12">

                                            <input type="text" autocomplete="off" class="form-control" value="@if(isset($entity)) {{$entity->name}} @endif" name="name" placeholder="Product Name"> </div>
                                    </div>

                                    <div class="form-group col-md-6" >

                                        <div class="col-md-12">

                                            <select name="category_id" class="form-control">
                                                <option>Categories</option>
                                                @foreach($categories as $category)

                                                    @if(isset($entity))
                                                        @if($entity->category_id == $category->id)
                                                            <option value="{{$category->id}}" selected="selected" >{{$category->name}}</option>
                                                        @else
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endif

                                                @endforeach
                                            </select>
                                    </div>
                                    </div>







                                        <div class="form-group col-md-6" >
                                            <label class="col-md-12"> Description:</label>
                                            <div class="col-md-12">
                                                <!--
                                                <textarea name="tm" class="form-control"></textarea> -->
                                                <textarea id="summernote" autocomplete="off"  name="description" rows="5">@if(isset($entity)) {{$entity->description}} @endif</textarea>
                                            </div>
                                        </div>


                                <div class="form-group col-md-6" >

                                    <div class="col-md-12">
                                        <input type="text" id="price" autocomplete="off"  name="price" value="@if(isset($entity)) {{$entity->price}} @endif" class="form-control" placeholder="Price"> </div>

                                    <div class="col-md-12">
                                        <input type="text" id="qty" autocomplete="off"  name="qty" value="@if(isset($entity)) {{$entity->qty}} @endif" class="form-control" placeholder="Quantity"> </div>
                                </div>

                                    <div class="form-group col-md-12" >
                                        <label class="col-md-3" for="example-email" id="thumblabel"> Product Image: </label>

                                        <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <a id="lfm" data-input="thumbnail" data-preview="holder2" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                  </span>
                                            <input id="thumbnail" class="form-control" type="text" name="image" value="@if(isset($entity)) {{$entity->image}}  @endif">
                                        </div>
                                    </div>


                                    <div class="form-group col-md-12" style="text-align: center;">


                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">  @if(isset($entity)) Save @else Add Product  @endif </button>

                                </div>

                            </div>


                            <div class="col-md-4">
                                @if(isset($entity))
                                    <img src="{{$entity->image}}" style="max-width: 300px; max-height:400px; box-shadow: 0px 0px 10px gray;" />
                                @endif
                                </div>

                        </form>
    </div>


@endsection