@extends('admin.layout.main')

@section('page_heading')
    إدارة المشاريع
@endsection

@section('breadcrumb')

    <ol class="breadcrumb">

        <li class="active">قائمة المشاريع</li>

        <li><a href="/admin">لوحة التحكم</a></li>
    </ol>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="panel-heading">{{$heading_title}}
                    <a href="/admin/device/create" class="btn btn-default btn-sm pull-right">+ اضافة مشروع جديد</a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover manage-u-table">
                        <thead>
                        <tr>
                            <!--
                            <th width="70" class="text-center">#</th> -->
                            <th>رقم المشروع</th>
                            <th>اسم المشروع</th>
                            <th>تاريخ انشاء المشروع</th>


                            <th width="300">تعديل</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $list_item)
                            <tr>
                                <!--
                            <td class="text-center">{{$list_item->id}}</td> -->
                                <td>{{$list_item->projectno}}
                                <td class="text-left">{{$list_item->projectname}}</td>

                                </td>

                                <td>{{$list_item->created_at}}


                                   

                                <td>
                                    <!--
                                    <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-key"></i></button> -->
                                    <a type="button" href="{{ URL('admin/device/delete/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-trash"></i></a>
                                    <a type="button"  href="{{ URL('admin/device/edit/'.$list_item->id )}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection