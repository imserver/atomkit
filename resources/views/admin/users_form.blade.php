@extends('admin.layout.main')

@section('page_heading')
   {{$heading_title}} - {{$subheading_title}}
@endsection

@section('breadcrumb')

    <ol class="breadcrumb">
        <li class="active">{{$subheading_title}}</li>
        <li><a href="/admin/device">{{$heading_title}}</a></li>
        <li><a href="/admin">Dashboard</a></li>
    </ol>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @if ($errors->any())
                <div class="panel">

                    <div class="panel-heading">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

        </div>
    </div>

    <div class="row">
        @if(isset($mthoad))
            @if($mthoad==1)
                <form method="post" action="{{ route('device.store') }}" class="form-horizontal" >
                    @elseif($mthoad==2)
                        <form method="post" action="{{ route('device.update') }}" class="form-horizontal" >
                            @endif
                            @endif
                            <div class="col-md-9">
                                <div class="white-box">
                                    <h3 class="box-title m-b-0">{{$heading_title}}</h3>
                                    <p class="text-muted m-b-30 font-13"> {{$subheading_title}}</p>
                                    <!-- <form class="form-horizontal"> -->
                                    {{ csrf_field() }}
                                    @if(isset($entity))
                                        <input autocomplete="off" type="hidden" name="id" value="{{$entity->id}}" />
                                    @endif

                                    <div class="form-group">
                                        <label class="col-md-12">Project Name:</label>
                                        <div class="col-md-12">

                                            <input type="text" autocomplete="off" class="form-control" value="@if(isset($entity)) {{$entity->projectname}} @endif" name="projectname" placeholder="Project Name"> </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12" for="example-email">Project No:</label>
                                        <div class="col-md-12">
                                            <input type="text" id="projectno" autocomplete="off" name="projectno" value="@if(isset($entity)) {{$entity->projectno}} @endif" class="form-control" placeholder="Project No"> </div>
                                    </div>






                                    <div class="form-group">
                                        <label class="col-md-12"> Missing Parts:</label>
                                        <div class="col-md-12">
                                            <!--
                                            <textarea name="tm" class="form-control"></textarea> -->
                                            <textarea id="summernote" autocomplete="off" name="missingparts" rows="10">@if(isset($entity)) {{$entity->missingpartsno}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12" for="example-email">Missing Parts No:</label>
                                        <div class="col-md-12">
                                            <input type="text" id="missingpartsno" autocomplete="off" name="missingpartsno" value="@if(isset($entity)) {{$entity->missingpartsno}} @endif" class="form-control" placeholder="Missing Parts No"> </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-12">Notes:</label>
                                        <div class="col-md-12">
                                            <!--
                                            <textarea name="tm" class="form-control"></textarea> -->
                                            <textarea id="summernote2" autocomplete="off" name="notes" rows="10">@if(isset($entity)) {{$entity->notes}} @endif</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-12"> Replaced Parts:</label>
                                        <div class="col-md-12">
                                            <!--
                                            <textarea name="tm" class="form-control"></textarea> -->
                                            <textarea id="summernote3" autocomplete="off" name="replacedparts" rows="10">@if(isset($entity)) {{$entity->replacedparts}} @endif</textarea>
                                        </div>
                                    </div>


                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                </div>
                            </div>


                        </form>
    </div>

@endsection